@extends('admin.layouts.app')

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
            Edit About
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{url('admin:home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Edit Category</li>
        </ol>
    </section>




    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-10 col-md-offset-1">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Category Form</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    {!! Form::model($category,['route'=>['admin:category.update'],'method'=>'POST','files'=>true,]) !!}

                    @include('admin.layouts.messages')

                    @csrf

                    <div class="box-body">




                        <div class="form-group">
                            <label for="exampleInputPassword1">Junior Section</label>
                            {!! Form::textarea('junior',null,['class'=>'form-control editor','placeholder'=>'Body','id'=>'editor']) !!}
                        </div>


                        <div class="form-group">
                            <label for="exampleInputPassword1">Senior Section</label>
                            {!! Form::textarea('senior',null,['class'=>'form-control editor','placeholder'=>'Body','id'=>'editor2']) !!}
                        </div>







                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    {{Form::close()}}

                </div>
                <!-- /.box -->


            </div>
        </div>
    </section>


@stop

@section('js')
    <script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
    <script>CKFinder.config( { connectorPath: '/ckfinder/connector' } );</script>
    <script>
        CKEDITOR.replace( 'editor' ,{
            extraPlugins: 'imageuploader'
        });
        CKEDITOR.replace( 'editor2' ,{
            extraPlugins: 'imageuploader'
        });
    </script>
@endsection
