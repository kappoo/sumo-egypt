@extends('admin.layouts.app')

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
            Edit Video
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{url('admin:home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Edit Video</li>
        </ol>
    </section>




    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-10 col-md-offset-1">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Video Form</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    {!! Form::model($video,['route'=>['admin:video.update'],'method'=>'POST','files'=>true,]) !!}

                    @include('admin.layouts.messages')

                    @csrf

                    <div class="box-body">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            {!! Form::textarea('body',null,['class'=>'form-control','placeholder'=>'title','id'=>'editor']) !!}
                        </div>




                        <div class="form-group">
                            <label for="exampleInputPassword1">Body</label>
                            {!! Form::text('video',null,['class'=>'form-control','placeholder'=>'title','id'=>'editor']) !!}
                        </div>




                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    {{Form::close()}}

                </div>
                <!-- /.box -->


            </div>
        </div>
    </section>


@stop

@section('js')

    <script>
        CKEDITOR.replace( 'editor' );
    </script>
@endsection
