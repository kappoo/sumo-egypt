
<div style="padding: 20px">
    @if(count($errors))
        <div class="alert alert-danger">
            <ul>

                {!!  implode('', $errors->all('<li class="error  ">:message</li>'))  !!}

            </ul>

        </div>
    @endif



    @if(Session::has('success')||Session::has('error'))

        <div class="alert alert-{{Session::has('success')?'success':'danger'}}">
           {!!  Session::has('success')? ' <i class="fa fa-correct"></i> '.Session::get('success'):Session::get('error')!!}
        </div>
    @endif

    @if(Session::has('info'))

        <div class="alert alert-info">
            <i class="fa fa-info-circle"></i> <span style="font-weight: 600;font-size: 14px;
            line-height: 21px;"><br/>{!!  Session::get('info')!!}</span>
        </div>
    @endif
</div>

