<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/dist/img/avatar5.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{@Auth::user()->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>











                    <li class="{{request()->url()==route('admin:home')?'active':''}}">
                        <a href="{{route('admin:dashboard')}}">
                            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                        </a>

                    </li>

                    <li class="{{request()->url()==route('admin:home')?'active':''}}">
                        <a href="{{route('admin:home')}}">
                            <i class="fa fa-home"></i> <span>Home</span>
                        </a>

                    </li>

                    <li class="{{request()->url()==route('admin:about')?'active':''}}">
                        <a href="{{route('admin:about')}}">
                            <i class="fa fa-info"></i> <span>About</span>
                        </a>

                    </li>


                    <li class="{{request()->url()==route('admin:video')?'active':''}}">
                        <a href="{{route('admin:video')}}">
                            <i class="fa fa-video-camera"></i> <span>Video</span>
                        </a>

                    </li>


                    <li class="{{request()->url()==route('admin:category')?'active':''}}">
                        <a href="{{route('admin:category')}}">
                            <i class="fa fa-cubes"></i> <span>Category</span>
                        </a>

                    </li>






        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
