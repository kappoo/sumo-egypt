
@extends('site.app')

<section id="register" class="parallax-section" style="background-position: 50% 40px;">




        <div class="container">
            <div class="row">

                @include('admin.layouts.messages')

                <div>
                    {!! $category->senior !!}
                </div>

                <div class="wow fadeInUp col-md-7 col-sm-7 animated" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                    <h2>Register Here</h2>
                    <h3>Nunc eu nibh vel augue mollis tincidunt id efficitur tortor. Sed pulvinar est sit amet tellus iaculis hendrerit.</h3>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet. Dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet consectetuer diam nonummy.</p>
                </div>

                <div class="wow fadeInUp col-md-5 col-sm-5 animated" data-wow-delay="1s" style="visibility: visible; animation-delay: 1s; animation-name: fadeInUp;">
                    <form action="#" method="post">
                        <input name="firstname" type="text" class="form-control" id="firstname" placeholder="First Name">
                        <input name="lastname" type="text" class="form-control" id="lastname" placeholder="Last Name">
                        <input name="phone" type="telephone" class="form-control" id="phone" placeholder="Phone Number">
                        <input name="email" type="email" class="form-control" id="email" placeholder="Email Address">
                        <div class="col-md-offset-6 col-md-6 col-sm-offset-1 col-sm-10">
                            <input name="submit" type="submit" class="form-control" id="submit" value="REGISTER">
                        </div>
                    </form>
                </div>

                <div class="col-md-1"></div>

            </div>
        </div>
</section>