
<!-- =========================
    VIDEO SECTION
============================== -->
<section id="video" class="parallax-section">
    <div class="container">
        <div class="row">

            <div class="wow fadeInUp col-md-6 col-sm-10" data-wow-delay="1.3s">
                {!! $video->body !!}
            </div>
            <div class="wow fadeInUp col-md-6 col-sm-10" data-wow-delay="1.6s">
                <div class="embed-responsive embed-responsive-16by9">
                    {!!  $video->video!!}
                </div>
            </div>

        </div>
    </div>
</section>
