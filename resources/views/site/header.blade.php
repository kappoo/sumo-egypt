<!-- =========================
     NAVIGATION LINKS
============================== -->
<div class="navbar navbar-fixed-top custom-navbar" role="navigation">
    <div class="container">

        <!-- navbar header -->
        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>
            <a href="{{url('/site')}}/#" class="navbar-brand">
                <img src="{{url('/images')}}/logo.png" width="50px" height="50px" alt="Egypt Sumo "/>
            </a>
        </div>

        <div class="collapse navbar-collapse">

            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{route('home')}}" class="smoothScroll">Home</a></li>

                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        Register <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                       <li><a href="{{route('register.junior')}}">Junior</a></li>
                        <li><a href="{{route('register.senior')}}">Senior</a></li>
                    </ul>
                </li>

            </ul>

        </div>

    </div>
</div>
