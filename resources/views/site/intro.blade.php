<!-- =========================
    INTRO SECTION
============================== -->
<section id="intro" class="parallax-section" style="background: url({{asset('storage/images/home/'.$home->photo)}}) 50% 0 repeat-y fixed !important;">
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-sm-12">
                <div class="body" >
                    {!! $home->title !!}
                </div>

            </div>


        </div>
    </div>
</section>
