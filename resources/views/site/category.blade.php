
<!-- =========================
    PROGRAM SECTION
============================== -->
<section id="program" class="parallax-section">
    <div class="container">
        <div class="row">

            <div class="wow fadeInUp col-md-12 col-sm-12" data-wow-delay="0.6s">
                <div class="section-title">
                    <h2>Category</h2>
                </div>
            </div>

            <div class="wow fadeInUp col-md-10 col-sm-10" data-wow-delay="0.9s">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#fday" aria-controls="fday" role="tab" data-toggle="tab">Junior</a></li>
                    <li><a href="#sday" aria-controls="sday" role="tab" data-toggle="tab">Senior</a></li>

                </ul>
                <!-- tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane active" id="fday">
                        {!! $category->junior !!}

                        <br/>
                        <a href="{{route('register.junior')}}" class="btn btn-lg btn-danger smoothScroll wow fadeInUp" data-wow-delay="1s">REGISTER NOW</a>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="sday">
                        {!! $category->senior !!}
                        <br/>
                        <a href="{{route('register.senior')}}" class="btn btn-lg btn-danger smoothScroll wow fadeInUp" data-wow-delay="1s">REGISTER NOW</a>
                    </div>



                </div>

            </div>
        </div>
    </div>
</section>
