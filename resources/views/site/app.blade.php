
<!DOCTYPE html>
<html lang="en">
<head>
    <!--
    New Event
    http://www.templatemo.com/tm-486-new-event
    -->
    <title>New Event - Responsive HTML Template</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="{{url('/site')}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{url('/site')}}/css/animate.css">
    <link rel="stylesheet" href="{{url('/site')}}/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{url('/site')}}/css/owl.theme.css">
    <link rel="stylesheet" href="{{url('/site')}}/css/owl.carousel.css">

    <!-- Main css -->
    <link rel="stylesheet" href="{{url('/site')}}/css/style.css">


    <link rel="stylesheet" href="{{url('/site')}}/css/custom.css">


    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Poppins:400,500,600' rel='stylesheet' type='text/css'>

</head>
<body data-spy="scroll" data-offset="50" data-target=".navbar-collapse">

<!-- =========================
     PRE LOADER
============================== -->
<div class="preloader">

    <div class="sk-rotating-plane"></div>

</div>


@include('site.header')




@yield('content')




@include('site.footer')

<!-- Back top -->
<a href="{{url('/site')}}/#back-top" class="go-top"><i class="fa fa-angle-up"></i></a>


<!-- =========================
     SCRIPTS
============================== -->
<script src="{{url('/site')}}/js/jquery.js"></script>
<script src="{{url('/site')}}/js/bootstrap.min.js"></script>
<script src="{{url('/site')}}/js/jquery.parallax.js"></script>
<script src="{{url('/site')}}/js/owl.carousel.min.js"></script>
<script src="{{url('/site')}}/js/smoothscroll.js"></script>
<script src="{{url('/site')}}/js/wow.min.js"></script>
<script src="{{url('/site')}}/js/custom.js"></script>

</body>
</html>