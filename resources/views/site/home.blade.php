
@extends('site.app')

@section('content')



    @include('site.intro')

    @include('site.about')

<!-- =========================
    DETAIL SECTION
============================== -->
<section id="detail" class="parallax-section">
    <div class="container">
        <div class="row">

            <div class="wow fadeInLeft col-md-4 col-sm-4" data-wow-delay="0.3s">
                <i class="fa fa-group"></i>
                <h3>650 Participants</h3>
                <p>Quisque ut libero sapien. Integer tellus nisl, efficitur sed dolor at, vehicula finibus massa. Sed tincidunt metus sed eleifend suscipit.</p>
            </div>

            <div class="wow fadeInUp col-md-4 col-sm-4" data-wow-delay="0.6s">
                <i class="fa fa-clock-o"></i>
                <h3>24 Programs</h3>
                <p>Quisque ut libero sapien. Integer tellus nisl, efficitur sed dolor at, vehicula finibus massa. Sed tincidunt metus sed eleifend suscipit.</p>
            </div>

            <div class="wow fadeInRight col-md-4 col-sm-4" data-wow-delay="0.9s">
                <i class="fa fa-microphone"></i>
                <h3>11 Speakers</h3>
                <p>Quisque ut libero sapien. Integer tellus nisl, efficitur sed dolor at, vehicula finibus massa. Sed tincidunt metus sed eleifend suscipit.</p>
            </div>

        </div>
    </div>
</section>



    @include('site.video')


    @include('site.category')

<!-- =========================
   REGISTER SECTION
============================== -->
<section id="map" class="parallax-section">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27643.79449175254!2d31.426575436644168!3d30.001476179930822!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa83dc3188a646ed!2siSchool!5e0!3m2!1sen!2seg!4v1535799296170" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>


<!-- =========================
    FAQ SECTION
============================== -->
<section id="faq" class="parallax-section">
    <div class="container">
        <div class="row">

            <!-- Section title
            ================================================== -->
            <div class="wow bounceIn col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10 text-center">
                <div class="section-title">
                    <h2>Do you have Questions?</h2>
                    <p>Lorem ipsum dolor sit amet, maecenas eget vestibulum justo imperdiet.</p>
                </div>
            </div>

            <div class="wow fadeInUp col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10" data-wow-delay="0.9s">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="{{url('/site')}}/#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    What is Responsive Design?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <p>Lorem ipsum dolor sit amet, maecenas eget vestibulum justo imperdiet, wisi risus purus augue vulputate voluptate neque, curabitur dolor libero sodales vitae elit massa. Lorem ipsum dolor sit amet, maecenas eget vestibulum justo imperdiet.</p>
                                <p>Nunc eu nibh vel augue mollis tincidunt id efficitur tortor. Sed pulvinar est sit amet tellus iaculis hendrerit. Mauris justo erat, rhoncus in arcu at, scelerisque tempor erat.</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="{{url('/site')}}/#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    What are latest UX Developments?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <p>Nunc eu nibh vel augue mollis tincidunt id efficitur tortor. Sed pulvinar est sit amet tellus iaculis hendrerit. Mauris justo erat, rhoncus in arcu at, scelerisque tempor erat.</p>
                                <p>Lorem ipsum dolor sit amet, maecenas eget vestibulum justo imperdiet, wisi risus purus augue vulputate voluptate neque, curabitur dolor libero sodales vitae elit massa. Lorem ipsum dolor sit amet, maecenas eget vestibulum justo imperdiet.</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="{{url('/site')}}/#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    What things about Social Media will be discussed?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                <p>Aenean vulputate finibus justo et feugiat. Ut turpis lacus, dapibus quis justo id, porttitor tempor justo. Quisque ut libero sapien. Integer tellus nisl, efficitur sed dolor at, vehicula finibus massa.</p>
                                <p>Lorem ipsum dolor sit amet, maecenas eget vestibulum justo imperdiet, wisi risus purus augue vulputate voluptate neque, curabitur dolor libero sodales vitae elit massa. Lorem ipsum dolor sit amet, maecenas eget vestibulum justo imperdiet.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>


<!-- =========================
    VENUE SECTION
============================== -->
<section id="venue" class="parallax-section">
    <div class="container">
        <div class="row">

            <div class="wow fadeInLeft col-md-offset-1 col-md-5 col-sm-8" data-wow-delay="0.9s">
                <h2>Venue</h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet. Dolore magna aliquam erat volutpat.</p>
                <h4>New Event</h4>
                <h4>120 Market Street, Suite 110</h4>
                <h4>San Francisco, CA 10110</h4>
                <h4>Tel: 010-020-0120</h4>
            </div>

        </div>
    </div>
</section>


<!-- =========================
    SPONSORS SECTION
============================== -->
<section id="sponsors" class="parallax-section">
    <div class="container">
        <div class="row">

            <div class="wow bounceIn col-md-12 col-sm-12">
                <div class="section-title">
                    <h2>Our Sponsors</h2>
                    <p>Lorem ipsum dolor sit amet, maecenas eget vestibulum justo imperdiet.</p>
                </div>
            </div>

            <div class="wow fadeInUp col-md-3 col-sm-6 col-xs-6" data-wow-delay="0.3s">
                <img src="{{url('/site')}}/images/sponsor-img1.jpg" class="img-responsive" alt="sponsors">
            </div>

            <div class="wow fadeInUp col-md-3 col-sm-6 col-xs-6" data-wow-delay="0.6s">
                <img src="{{url('/site')}}/images/sponsor-img2.jpg" class="img-responsive" alt="sponsors">
            </div>

            <div class="wow fadeInUp col-md-3 col-sm-6 col-xs-6" data-wow-delay="0.9s">
                <img src="{{url('/site')}}/images/sponsor-img3.jpg" class="img-responsive" alt="sponsors">
            </div>

            <div class="wow fadeInUp col-md-3 col-sm-6 col-xs-6" data-wow-delay="1s">
                <img src="{{url('/site')}}/images/sponsor-img4.jpg" class="img-responsive" alt="sponsors">
            </div>

        </div>
    </div>
</section>


<!-- =========================
    CONTACT SECTION
============================== -->
<section id="contact" class="parallax-section">
    <div class="container">
        <div class="row">

            <div class="wow fadeInUp col-md-offset-1 col-md-5 col-sm-6" data-wow-delay="0.6s">
                <div class="contact_des">
                    <h3>New Event</h3>
                    <p>Proin sodales convallis urna eu condimentum. Morbi tincidunt augue eros, vitae pretium mi condimentum eget. Suspendisse eu turpis sed elit pretium congue.</p>
                    <p>Mauris at tincidunt felis, vitae aliquam magna. Sed aliquam fringilla vestibulum. Praesent ullamcorper mauris fermentum turpis scelerisque rutrum eget eget turpis.</p>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet. Dolore magna aliquam erat volutpat. Lorem ipsum dolor.</p>
                    <a href="{{url('/site')}}/#" class="btn btn-danger">DOWNLOAD NOW</a>
                </div>
            </div>

            <div class="wow fadeInUp col-md-5 col-sm-6" data-wow-delay="0.9s">
                <div class="contact_detail">
                    <div class="section-title">
                        <h2>Keep in touch</h2>
                    </div>
                    <form action="#" method="post">
                        <input name="name" type="text" class="form-control" id="name" placeholder="Name">
                        <input name="email" type="email" class="form-control" id="email" placeholder="Email">
                        <textarea name="message" rows="5" class="form-control" id="message" placeholder="Message"></textarea>
                        <div class="col-md-6 col-sm-10">
                            <input name="submit" type="submit" class="form-control" id="submit" value="SEND">
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</section>

@endsection