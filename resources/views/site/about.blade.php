<!-- =========================
    OVERVIEW SECTION
============================== -->
<section id="overview" class="parallax-section">
    <div class="container">
        <div class="row">

            <div class="wow fadeInUp col-md-6 col-sm-6" data-wow-delay="0.6s">
                <h3>{{$about->title}}</h3>
                {!!  $about->body!!}
            </div>

            <div class="wow fadeInUp col-md-6 col-sm-6" data-wow-delay="0.9s">
                <img src="{{asset('storage/images/about/'.$about->photo)}}" class="img-responsive" alt="Overview">
            </div>

        </div>
    </div>
</section>
