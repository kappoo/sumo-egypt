/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];

	
	
	    
	    
		config.extraPlugins = 'colorbutton,font,justify,youtube,autogrow,bootstrapwidgets,ckeditor-gwf-plugin,imageresize,uploadimage,uploadwidget,widget,clipboard,filetools,notification,toolbar,button';
		config.font_names =
				'Arial/Arial, Helvetica, sans-serif;' +
			    'Times New Roman/Times New Roman, Times, serif;' +
			    'Verdana;'+
			    'tahoma;'+
			    'georgia;'+
			    'GoogleWebFonts';	
		config.colorButton_colors = 'CF5D4E,454545,FFF,000,CCC,DDD,CCEAEE,66AB16,20B8E2,FFEB3B,795548,607D8B,2196F3,3F51B5,03A9F4,00BCD4,FFEB3B,FFC107,FF5722,E91E63,9C27B0';
		config.colorButton_enableAutomatic = false;
	
	
	
	
	
	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	//config.removeButtons = 'Underline,Subscript,Superscript';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	
	//config.removeDialogTabs = 'image:advanced;link:advanced';
	
	config.filebrowserImageBrowseUrl = '/assets/kcfinder/browse.php?opener=ckeditor&type=images';
    config.filebrowserImageUploadUrl = '/assets/kcfinder/upload.php?opener=ckeditor&type=images';
    config.uploadUrl = '/assets/kcfinderdrag/upload.php?opener=ckeditor&type=images';
    config.imageUploadUrl = '/assets/kcfinderdrag/upload.php?opener=ckeditor&type=images';

};
