<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name'=>'Admin',
            'username'=>'test',
            'is_admin'=>true,
            'password'=>bcrypt('test')
        ]);


        \App\About::create(['title'=>'About','body'=>'body','photo'=>'s']);

        \App\Home::create(['title'=>'Home','photo'=>'s']);

        \App\Video::create(['body'=>'video','video'=>'s']);

        \App\Category::create(['senior'=>'senior','junior'=>'junior']);
    }
}
