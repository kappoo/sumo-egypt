<?php

use Intervention\Image\Facades\Image;

function create($class,$attributes=[],$times=null)
{


    return factory($class,$times)->create($attributes);


}


function create_random_name() {

    $time       = time();
    $random     = rand( 1, 100000 );
    $divide     = $time / $random;
    $encryption = md5( $divide );
    $name_enc   = substr( $encryption, 0, 20 );

    return $name_enc;
}


 function uploadImage($image,$directory)
{

    $fileName    =  create_random_name() . '.' . $image->getClientOriginalExtension();

    $img = Image::make($image->getRealPath());
    $img->stream();

    Storage::disk('local')->put("public/images/{$directory}/".$fileName,$img);

    return $fileName;
}


function getUrl($link)
{
    if(\Route::has($link))
    return route($link);

    return url('/'.$link);
}



