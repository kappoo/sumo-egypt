<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {






        if (\Auth::guest ()) {

            if (\Request::ajax ()) {
                return Response::make ( 'Unauthorized', 401 );
            } else {

                return \Redirect::route ( 'admin:login' );
            }
        }
        // get the logged userd
            $user_is_admin = \Auth::user()->is_admin ? true:false ;




        if (! $user_is_admin ) {
            \Auth::logout();
            return \Redirect::route ( 'admin:login' );
        }


        return $next($request);
    }
}
