<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use View;
use Validator;
use Session;
use Redirect;
use Auth;

class AdminLoginController extends Controller

{



    /**

     * User Repository

     *

     * @var User

     */

    protected $user;

    public $data = array();

    public function login()

    {




    	Auth::logout();




        $input = Input::all();

        if (!empty($input )) {

            $validation = Validator::make($input, ['username' => 'required', 'password' => 'required']);

            if ($validation->passes()) {



                $remember_user = false;

                if (Input::get('remember_me') == 'Remember Me') {

                    $remember_user = true;

                }




                $LoginParams = ['username' => Input::get('username'), 'password' => Input::get('password') ];





                if (Auth::attempt($LoginParams, $remember_user)) {




                    $user_is_admin = \Auth::user()->is_admin ? true:false ;

                    if($user_is_admin)
                    Session::flash('success', 'Welcome admin');









                	return Redirect::route('admin:home');



                }else{



                    Session::flash('error', 'Wrong data,try again');





                }



            }

            return Redirect::route('admin:login');

        }








        return View::make('admin.auth.login');

    }



    public function logout()

    {

        Session::put('error', 'You have  logged out');


        Auth::logout();

        return Redirect::to('/admin');

    }







}