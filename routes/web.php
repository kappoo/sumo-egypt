<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>'web'],function (){






    Route::group
    (
        ['namespace'=>'Site' ],function ()
        {



            Route::get('/', array('as' => 'home', 'uses' => 'HomeController@home'));


            Route::get('/register-junior', array('as' => 'register.junior', 'uses' => 'RegisterController@juniorForm'));

            Route::get('/register-senior', array('as' => 'register.senior', 'uses' => 'RegisterController@seniorForm'));




        }
    );

    Route::group(
            [
                'prefix'=>'admin-panel',
                 'namespace'=>'Admin',
                'as'=>'admin:'
             ],function ()
        {



            Route::get('/login', array('as' => 'login', 'uses' => 'AdminLoginController@login'));
            Route::post( '/login', [ 'as' => 'login_post', 'uses' => 'AdminLoginController@login' ] );
            Route::get( '/logout', array('as' => 'logout', 'uses' => 'AdminLoginController@logout') );


            Route::group(['middleware'=>['admin']],function (){







                Route::get('/',[
                    'as'=>'dashboard',
                    'uses'=>'DashboardController@index'
                ]);


                Route::get('/home',[
                    'as'=>'home',
                    'uses'=>'HomeController@home'
                ]);

                Route::post('/home/update',[
                    'as'=>'home.update',
                    'uses'=>'HomeController@update'
                ]);


                Route::get('/about',[
                    'as'=>'about',
                    'uses'=>'AboutController@about'
                ]);

                Route::post('/about/update',[
                    'as'=>'about.update',
                    'uses'=>'AboutController@update'
                ]);



                Route::get('/video',[
                    'as'=>'video',
                    'uses'=>'VideoController@video'
                ]);

                Route::post('/video/update',[
                    'as'=>'video.update',
                    'uses'=>'VideoController@update'
                ]);



                Route::get('/category',[
                    'as'=>'category',
                    'uses'=>'CategoryController@category'
                ]);

                Route::post('/category/update',[
                    'as'=>'category.update',
                    'uses'=>'CategoryController@update'
                ]);



            });

        }
    );





});
